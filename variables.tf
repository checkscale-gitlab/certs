variable "domain_name" {
    description = "Domain of the application"
    type        = string
}

variable "name" {
    description = "App name"
    type        = string
}

variable "type" {
    description = "Api or Static"
    type        = string
}
